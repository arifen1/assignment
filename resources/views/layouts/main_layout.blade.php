<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>



<body>

    <div class="main-wrapper">

        @include('includes.header')

        <div class="main-content-wrap section-ptb product-details-page">
            @yield('content')
        </div>

    </div>

    @include('includes.footer')

    @include('includes.script')

</body>

</html>