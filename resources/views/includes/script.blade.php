    <!-- jquery -->
    <script src="{{ asset('/assets/js/vendor/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('/assets/js/vendor/jquery-migrate-3.3.0.min.js') }}"></script>
    <!-- Popper JS -->
    <script src="{{ asset('/assets/js/popper.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
    <!-- Plugins JS -->
    <script src="{{ asset('/assets/js/plugins.js') }}"></script>
    <!-- Main JS -->
    <script src="{{ asset('/assets/js/main.js') }}"></script>