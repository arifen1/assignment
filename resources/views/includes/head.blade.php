    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Product Details</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS 
    ========================= -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="{{ asset('/assets/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/ionicons.min.css') }}">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('/assets/css/plugins.css') }}">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">

    <!-- Custom Style CSS -->
    <link rel="stylesheet" href="{{ asset('/assets/css/custom.css') }}">