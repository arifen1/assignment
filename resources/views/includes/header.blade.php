        <!-- header-area start -->
        <div class="header-area">

            <div class="header-bottom-area header-sticky">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-5 col-5">
                            <div class="logo">
                                <a href="#"><img src="{{ asset('assets/images/logo.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-8 d-none d-lg-block">
                            <div class="main-menu-area text-center">
                                <nav class="main-navigation">
                                    <ul>
                                        <li class="active"><a href="#">Home</a></li>
                                        <li class=""><a href="#">Link One</a></li>
                                        <li class=""><a href="#">Link Two</a></li>
                                        <li class=""><a href="#">Link Three</a></li>
                                        <li class=""><a href="#">Link Four</a></li>


                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-7 col-7">
                            <div class="right-blok-box d-flex">


                                <div class="shopping-cart-wrap">
                                    <a href="#"><i class="icon-handbag"></i> <span id="cart-total">2</span></a>
                                    <ul class="mini-cart">
                                        <li class="cart-item">
                                            <div class="cart-image">
                                                <a href="#"><img alt="" src="{{ asset('assets/images/products/w1.jpg')}}"></a>
                                            </div>
                                            <div class="cart-title">
                                                <a href="single-product.html">
                                                    <h4>Product Name 01</h4>
                                                </a>
                                                <span class="quantity">1 ×</span>
                                                <div class="price-box"><span class="new-price">$130.00</span></div>
                                                <a class="remove_from_cart" href="#"><i class="icon-trash icons"></i></a>
                                            </div>
                                        </li>
                                        <li class="cart-item">
                                            <div class="cart-image">
                                                <a href="#l"><img alt="" src="{{ asset('assets/images/products/w2.jpg')}}"></a>
                                            </div>
                                            <div class="cart-title">
                                                <a href="single-product.html">
                                                    <h4>Product Name 03</h4>
                                                </a>
                                                <span class="quantity">1 ×</span>
                                                <div class="price-box"><span class="new-price">$130.00</span></div>
                                                <a class="remove_from_cart" href="#"><i class="icon-trash icons"></i></a>
                                            </div>
                                        </li>
                                        <li class="subtotal-titles">
                                            <div class="subtotal-titles">
                                                <h3>Sub-Total :</h3><span>$ 230.99</span>
                                            </div>
                                        </li>
                                        <li class="mini-cart-btns">
                                            <div class="cart-btns">
                                                <a href="#">View cart</a>
                                                <a href="#">Checkout</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mobile-menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header-area end -->