@extends('layouts/main_layout')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-xl-7 col-lg-7 col-md-6">
            <div class="product-details-images">
                <div class="product_details_container">
                    <!-- product_big_images start -->
                    <div class="product_big_images-top">

                        <div class="discount">
                            <div class="discount-label red"> <span>-10%</span> </div>
                        </div>


                        <div class="portfolio-full-image tab-content">




                            <?php foreach ($mdata['dimensions'][1]['images'] as $key => $data) { ?>
                                <div role="tabpanel" class="tab-pane product-image-position <?php echo $key === 0 ? 'active' : '' ?>" id="img-tab-<?php echo $key + 1; ?>">
                                    <a href="<?php echo $data ?>" class="img-poppu">
                                        <img src="<?php echo $data ?>" alt="#">
                                    </a>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <!-- product_big_images end -->

                    <!-- Start Small images -->
                    <ul class="product_small_images-bottom horizantal-product-active nav" role="tablist">

                        <?php foreach ($mdata['dimensions'][0]['images'] as $key => $data) { ?>
                            <li role="presentation" class="pot-small-img active">
                                <a href="#img-tab-<?php echo $key + 1; ?>" role="tab" data-bs-toggle="tab">
                                    <img src="<?php echo $data ?>" alt="small-image">
                                </a>
                            </li>
                        <?php } ?>

                    </ul>
                    <!-- End Small images -->
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-lg-5 col-md-6">
            <!-- product_details_info start -->
            <div class="product_details_info">
                <h2>"Russell Hobbs'' Retro elektrinis virdulys "Classic Noir" 21671-70</h2>

                <!-- pro_details start -->
                <div class="pro_details">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod temf incididunt ut labore et dolore magna aliqua.</p>
                </div>

                <div class="product_desc">
                    <div class="product_row">
                        <span class="product_name">Lorem ipsum dolor sit amet consectetur adipisicing elit</span>
                    </div>

                    <div class="product_row">
                        <span class="product_name">Lorem ipsum dolor sit amet consectetur adipisicing elit</span>
                    </div>

                    <div class="product_row">
                        <span class="product_name">Lorem ipsum dolor sit amet consectetur adipisicing elit</span>
                    </div>
                </div>


                <div class="product-space">

                    <ul class="pro_dtl_prize">
                        <li class="old_prize">€ 62.90</li>
                        <li> Lorem ipsum dolor sit</li>
                    </ul>

                    <div class="product-quantity">

                        <div class="main_price">€ 56,06</div>

                        <div class="price_block">

                            <span class="options">Kiekis</span>


                            <div class="input-group control-group">
                                <div class="input-group-btn">
                                    <button id="down" class="btn btn-default price-control"><i class="icon-minus"></i> </button>
                                </div>
                                <input type="text" class="input-number price-number" value="1" />
                                <div class="input-group-btn">
                                    <button id="up" class="btn btn-default price-control"><i class="icon-plus"></i></button>
                                </div>
                            </div>


                        </div>


                    </div>

                </div>




                <!-- pro_dtl_btn start -->
                <ul class="pro_dtl_btn">
                    <li><a href="#" class="buy_now_btn">buy now</a></li>
                </ul>
                <!-- pro_dtl_btn end -->

                <div class="pro_extra">

                    <div class="wishlist-control">
                        <i class="icon-heart"></i>
                        <span>Wishlist</span>
                    </div>
                    <div class="fb-share">
                        <i class="icon-social-facebook"></i> Share
                    </div>

                </div>

            </div>
            <!-- product_details_info end -->
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="product-details-tab mt--60">
                <ul role="tablist" class="mb--50 nav">
                    <li class="active" role="presentation">
                        <a data-bs-toggle="tab" role="tab" href="#description" class="active">Item Description</a>
                    </li>
                    <li role="presentation">
                        <a data-bs-toggle="tab" role="tab" href="#sheet">Detailed product</a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="col-12">
            <div class="product_details_tab_content tab-content">
                <!-- Start Single Content -->
                <div class="product_tab_content tab-pane active" id="description" role="tabpanel">
                    <div class="product_description_wrap">
                        <div class="product_desc mb--30">
                            <h2 class="title_3">Details</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis noexercit ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
                                id.
                            </p>
                        </div>
                        <div class="pro_feature">
                            <h2 class="title_3">Features</h2>
                            <ul class="feature_list">
                                <li><a href="#"><i class="ion-ios-play-outline"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                <li><a href="#"><i class="ion-ios-play-outline"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                <li><a href="#"><i class="ion-ios-play-outline"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Content -->
                <!-- Start Single Content -->
                <div class="product_tab_content tab-pane" id="sheet" role="tabpanel">
                    <div class="pro_feature">
                        <h2 class="title_3">Data sheet</h2>
                        <ul class="feature_list">
                            <li><a href="#"><i class="ion-ios-play-outline"></i>Duis aute irure dolor in reprehenderit in voluptate velit esse</a></li>
                            <li><a href="#"><i class="ion-ios-play-outline"></i>Irure dolor in reprehenderit in voluptate velit esse</a></li>
                            <li><a href="#"><i class="ion-ios-play-outline"></i>Irure dolor in reprehenderit in voluptate velit esse</a></li>
                            <li><a href="#"><i class="ion-ios-play-outline"></i>Sed do eiusmod tempor incididunt ut labore et </a></li>
                            <li><a href="#"><i class="ion-ios-play-outline"></i>Sed do eiusmod tempor incididunt ut labore et </a></li>
                            <li><a href="#"><i class="ion-ios-play-outline"></i>Nisi ut aliquip ex ea commodo consequat.</a></li>
                            <li><a href="#"><i class="ion-ios-play-outline"></i>Nisi ut aliquip ex ea commodo consequat.</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Single Content -->

            </div>
        </div>
    </div>
</div>

@stop