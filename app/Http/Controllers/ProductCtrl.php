<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class ProductCtrl extends Controller
{
    private $token;

    public function __construct()
    {

        if (!session()->has('access_token')) {
            $res = $this->auth();
            $this->token = $res;
        } else {
            $this->token = request()->session()->get('access_token');
        }
    }

    public function show(Request $request)
    {

        $mdata = $this->getItem($request->id);
        return view('pages.product-details', compact('mdata'));
    }


    private function auth()
    {
        $response =
            Http::post('https://gallery-api.engine.lt/api/auth/login', [
                'email' => 'arifen@gmail.com',
                'password' => 'arifen@21',
            ]);

        session()->put('access_token', $response['access_token']);
        return  $response['access_token'];
    }

    private function getItem($id)
    {
        $response =
            Http::withToken($this->token)->get('https://gallery-api.engine.lt/api/gallery/' . $id);
        return  $response;
    }
}
